const io = require("socket.io").listen(8081);

io.sockets.on("connection", function (socket) {
  socket.emit("connected", { event: "connected", name: "Hei" });

  socket.on("userAuth", (data) => {
    console.log(data);
  });
  socket.on("myData", (data) => {
    console.log(data);
  });
  socket.on("message", (data) => {
    console.log(data);
  });
  console.log("Connect");
});
