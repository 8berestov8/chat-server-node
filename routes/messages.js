const express = require("express");
const router = express.Router();
const connDB = require("../modules/connect");

/**
 * Создание сообщения
 */
router.post("/add_message", (req, res) => {
  connDB(req, res, (db) => {
    const dbase = db.db("chat");
    let uid = req.body.uid,
      resultJson = {
        isErrors: false,
        strErrors: [],
      };
    dbase.collection("sessions").findOne({ uid: uid }, (err, result) => {
      if (result) {
        let msg = req.body.msg,
          date = req.body.date,
          name = req.body.name,
          id_chat = req.body.id_user.toString() + result.id_user.toString();
        doc = {
          date: date,
          msg: msg,
          name: name,
          id_chat: id_chat,
        };
        dbase.collection("messages").insertOne(doc, (err, result) => {
          if (err) {
            resultJson.isErrors = true;
            resultJson.strErrors.push(
              `Ошибка подключения к базе данных ${err}`
            );
            res.json(resultJson);
            // return console.log(err);
          }
          db.close();
          res.json(resultJson);
        });
      } else {
        resultJson.isErrors = true;
        resultJson.strErrors.push(`Сессия не зарегистрирована!`);
        res.json(resultJson);
        // return console.log(err);
      }
      if (err) {
        resultJson.isErrors = true;
        resultJson.strErrors.push(`Ошибка подключения к базе данных ${err}`);
        res.json(resultJson);
        // return console.log(err);
      }
    });
  });
});
/**
 * Выводим все сообщения
 */
router.post("/get_list", (req, res) => {
  connDB(req, res, (db) => {
    const dbase = db.db("chat");
    let uid = req.body.uid,
      resultJson = {
        isErrors: false,
        strErrors: [],
        messages: [],
      };
    dbase.collection("sessions").findOne({ uid: uid }, (err, item) => {
      if (item) {
        resultJson.isErrors = false;
        const id_chat = item.id_user.toString() + req.body.id_user;
        const id_chat2 = req.body.id_user + item.id_user.toString();
        dbase
          .collection("messages")
          .find({
            $or: [{ id_chat: id_chat }, { id_chat: id_chat2 }],
          })
          .toArray((err, result) => {
            if (err) {
              resultJson.isErrors = true;
              resultJson.strErrors.push(
                `Ошибка подключения к базе данных ${err}`
              );
              res.json(resultJson);
              // return console.log(err);
            }
            db.close();
            resultJson.messages = result;
            res.json(resultJson);
            // res.json(resultJson);
          });
      } else {
        resultJson.isErrors = true;
        resultJson.strErrors.push(`Сессия не зарегистрирована!`);
        res.json(resultJson);
        // return console.log(err);
      }
      if (err) {
        resultJson.isErrors = true;
        resultJson.strErrors.push(`Ошибка подключения к базе данных ${err}`);
        res.json(resultJson);
        // return console.log(err);
      }
    });
  });
});
module.exports = router;
