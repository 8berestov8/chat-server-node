const express = require('express');
const router = express.Router();
const UIDGenerator = require('uid-generator');
const uidgen = new UIDGenerator();
const connDB = require('../modules/connect');
const sha = require('../modules/hash');
/**
	* Проверка пользователя
 */
router.post('/check_auth', (req, res) => {
	connDB(req, res, db => {
    const dbase = db.db("chat");
    let uid = req.body.uid,
        resultJson = {
          isErrors: false,
          strErrors: [],
          name: '',
          id_user: null,
          login: null
        };
    dbase.collection("sessions").findOne({uid: uid}, (err, item) => {
      if (item){
        resultJson.name = item.name;
        resultJson.id_user = item.id_user;
        resultJson.login = item.login;
        db.close();
        res.json(resultJson);
      } else {
        resultJson.isErrors = true;
        resultJson.strErrors.push('Сессия не найдена!');
        res.json(resultJson);
        // return console.log(err);
      }
      if (err) {
        resultJson.isErrors = true;
        resultJson.strErrors.push(`Ошибка подключения к базе данных ${err}`);
        res.send(resultJson);
        // return console.log(err);
      }
    });
  });
});
/**
	* Авторизация пользователя
 */
router.post('/auth', (req, res) => {
	connDB(req, res, db => {
    const dbase = db.db("chat");
    const uid = uidgen.generateSync();
    const login = typeof req.body.login === 'undefined'? null: req.body.login.trim(),
          pass = typeof req.body.pass === 'undefined'? null: req.body.pass.trim();
    let resultJson = {
      isErrors: false,
      strErrors: [],
      uid: null
    };
    if (login === null || login !== null && login.length === 0) {
      resultJson.isErrors = true;
      resultJson.strErrors.push(`Введите логин!`);
    }
    if (pass === null || pass !== null && pass.length === 0) {
      resultJson.isErrors = true;
      resultJson.strErrors.push(`Введите пароль!`);
    }
    if (!resultJson.isErrors) {
      let user = { 
        login: login,
        pass: sha(pass)
      };
      dbase.collection("users").findOne(user, (err, item) => {
        if (item) {      
          let session = {
            uid: uid,
            login: login,
            name: item.name,
            id_user: item._id
          };
					dbase.collection("sessions").insertOne(session, (err, result) => {
            if (err) {
              res.json(`Ошибка подключения к базе данных ${err}`);
              // return console.log(err);
            }
            db.close();
              resultJson.uid = uid;
              resultJson.name = session.name;
              resultJson.id_user = session.id_user;
              res.json(resultJson);
          });
        } else {
          resultJson.isErrors = true
          resultJson.strErrors.push(`Такой пользователь не зарегистрирован!`);
          res.json(resultJson);
          // return console.log(err);
        }
      });
    } else {
			res.json(resultJson)
		}
  });
});
/**
 	* Регистрация нового пользователя
 */
router.post('/reg', (req, res) => {
	connDB(req, res, db => {
		const dbase = db.db("chat");
    let name = typeof req.body.name === 'undefined'? null: req.body.name.trim(),
        login = typeof req.body.login === 'undefined'? null: req.body.login.trim(),
				pass = typeof req.body.pass === 'undefined'? null: req.body.pass.trim(),
				resultJson = {
					isErrors: false,
					strErrors: [],
        };
    if (name === null || name !== null && name.length === 0) {
      resultJson.isErrors = true;
      resultJson.strErrors.push(`Введите Имя!`);
    }
		if (login === null || login !== null && login.length === 0) {
			resultJson.isErrors = true;
			resultJson.strErrors.push(`Введите логин!`);
    }
		if (pass === null || pass !== null && pass.length === 0) {
			resultJson.isErrors = true;
			resultJson.strErrors.push(`Введите пароль!`);
		}
		if (!resultJson.isErrors) {
      const doc = {
        name: name,
        login: login,
        pass: sha(pass)
      };
			dbase.collection("users").insertOne(doc, (err, result) => {
				if (err) {
          resultJson.isErrors = true;
			    resultJson.strErrors.push(`Ошибка подключения к базе данных ${err}`);
					res.json(resultJson);
					// return console.log(err);
        }
				db.close();
				res.json(resultJson);
			});
		} else {
      resultJson.isErrors = true;
		  res.json(resultJson);
      // return console.log(err);
		}
	});
});
/**
 	* Вывод пользователей
 */
router.post('/get_users', (req, res) => {
  connDB(req, res, db => {
    const dbase = db.db("chat");
    let uid = req.body.uid,
        resultJson = {
          isErrors: false,
          strErrors: [],
          users: []
        };
    dbase.collection("sessions").findOne({uid: uid}, (err, item) => {
      if (item){
        dbase.collection("users").find({}).toArray((err, result) => {
          if (err) {
            resultJson.isErrors = true;
            resultJson.strErrors.push(`Ошибка подключения к базе данных ${err}`);
            res.json(resultJson);
            return console.log(err);
          }
          resultJson.users = result
          db.close();
          res.json(resultJson);
        });
      } else {
        resultJson.isErrors = true;
        resultJson.strErrors.push(`Ошибка, повторите еще раз!`);
        res.json(resultJson);
        return console.log(err);
      }
      if (err) {
        resultJson.isErrors = true;
        resultJson.strErrors.push(`Ошибка подключения к базе данных ${err}`);
        res.json(resultJson);
        return console.log(err);
      }
    });
  });
});

/**
 	* Выход пользователя
 */
router.post('/log_auth', (req, res) => {
	connDB(req, res, db => {
    const dbase = db.db("chat");
    let uid = req.body.uid,
        resultJson = {
          isErrors: false,
          strErrors: [],
        };
    dbase.collection("sessions").findOne({uid: uid}, (err, item) => {
      if (item){
        dbase.collection("sessions").deleteOne(item,(err, item) => {
          if (err) {
            resultJson.isErrors = true;
			      resultJson.strErrors.push(`Ошибка подключения к базе данных ${err}`);
            res.json(resultJson);
            return console.log(err);
          }
          db.close();
          res.json(resultJson);
        });
      } else {
        resultJson.isErrors = true;
        resultJson.strErrors.push(`Ошибка, повторите еще раз!`);
        res.json(resultJson);
        return console.log(err);
      }
      if (err) {
        resultJson.isErrors = true;
        resultJson.strErrors.push(`Ошибка подключения к базе данных ${err}`);
        res.json(resultJson);
        return console.log(err);
      }
    });
  });
});
module.exports = router;